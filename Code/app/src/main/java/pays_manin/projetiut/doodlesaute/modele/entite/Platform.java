package pays_manin.projetiut.doodlesaute.modele.entite;

import android.graphics.drawable.BitmapDrawable;

import pays_manin.projetiut.doodlesaute.modele.gestionCollision.CollisionPlatform;

public class Platform extends Entite {

    public Platform(BitmapDrawable bitmap, int x1, int y1, int x2, int y2) {
        super(bitmap,x1,y1);
        this.entiteLargeur = x2-x1;
        this.entiteHauteur = 50;
        this.laCollision = new CollisionPlatform(this);

    }


}
