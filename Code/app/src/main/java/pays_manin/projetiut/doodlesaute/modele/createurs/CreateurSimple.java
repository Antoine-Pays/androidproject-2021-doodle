package pays_manin.projetiut.doodlesaute.modele.createurs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.util.Random;

import pays_manin.projetiut.doodlesaute.R;
import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Joueur;
import pays_manin.projetiut.doodlesaute.modele.entite.Platform;
import pays_manin.projetiut.doodlesaute.view.GameView;

public class CreateurSimple extends CreateurEntite{

    @Override
    public Joueur creerJoueur(Partie partie, Context context) {

        Drawable drawable = context.getResources().getDrawable(R.mipmap.personnage);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        BitmapDrawable bitmapDrawable =  new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, 200, 200,true));

        Joueur j = new Joueur(bitmapDrawable, 500, 200, 700, 400);
        partie.ajouterEntite(j);
        return j;
    }

    @Override
    public void creerPlatform(Partie partie, Context context) {

        Drawable drawable = context.getResources().getDrawable(R.mipmap.plateforme);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        BitmapDrawable bitmapDrawable =  new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap, 200, 50,true));

        Random random = new Random();
        int randomX = 0;
        try {
            randomX = random.nextInt(GameView.largeurEcran - 201);
        } catch (Exception e) {
        }
        
        Platform platform = new Platform(bitmapDrawable, randomX, 0, randomX+200, 400);
        partie.ajouterEntite(platform);

    }
}
