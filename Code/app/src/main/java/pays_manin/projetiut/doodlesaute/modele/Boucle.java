package pays_manin.projetiut.doodlesaute.modele;

import android.graphics.Canvas;
import android.util.Log;

import pays_manin.projetiut.doodlesaute.view.GameView;

public class Boucle extends Thread {

    private static final int IPS = 30;

    private static final int IP1000 = 1000/IPS;

    private final GameView view;
    private boolean running = false;

    public Boucle(GameView view) {
        this.view = view;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    @Override
    public void run() {
        long debut;
        long pause;

        while (running){
            debut = System.currentTimeMillis();

            synchronized (view.getHolder()) {
                view.update();
            }

            Canvas c = null;
            try{
                c = view.getHolder().lockCanvas();
                synchronized (view.getHolder()){
                    view.doDraw(c);
                }
            }finally {
                if (c != null) {
                    view.getHolder().unlockCanvasAndPost(c);
                }
            }

            pause = IP1000-(System.currentTimeMillis() - debut);

            try {
                if (pause >= 0) {
                    sleep(pause);
                }
            }catch (Exception e){
                Log.d("TEST", "Exception : " + e);
            }
        }
    }
}
