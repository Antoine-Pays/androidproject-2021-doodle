package pays_manin.projetiut.doodlesaute.modele.gestionDeplacement;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;

public class DeplacementStandard extends GestionnaireDeplacement {

    public DeplacementStandard(Direction dir, Partie partie) {
        super(dir, partie);
    }

    protected int oldX;
    protected int oldY;
    protected int newX;
    protected int newY;

    @Override
    public void deplacer(Entite e) {

    }

    public int getOldX() {
        return oldX;
    }

    public int getOldY() {
        return oldY;
    }

    public int getNewX() {
        return newX;
    }

    public int getNewY() {
        return newY;
    }

}

