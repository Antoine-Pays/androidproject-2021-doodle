package pays_manin.projetiut.doodlesaute.modele;

import java.io.Serializable;

public class Utilisateur implements Serializable {
    private String motDePasse;
    private String pseudo;
    private int record;

    public Utilisateur( String pseudo, String motDePAsse, int record) {
        this.pseudo = pseudo;
        this.motDePasse = motDePAsse;
        this.record = record;
    }

    public Utilisateur( String pseudo, String motDePAsse) {
        this.pseudo = pseudo;
        this.motDePasse = motDePAsse;
        this.record = 0;
    }

    public String getPseudo() {
        return pseudo;
    }

    public int getRecord() {
        return record;
    }
    public void setRecord(int record) {
        this.record = record;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Utilisateur that = (Utilisateur) o;
        return motDePasse.equals(that.motDePasse) &&
                pseudo.equals(that.pseudo);
    }

    @Override
    public String toString() {
        String s = pseudo + "      "  + record;
        return s;
    }

}
