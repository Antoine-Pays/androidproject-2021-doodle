package pays_manin.projetiut.doodlesaute.modele.createurs;

import android.content.Context;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Joueur;

public abstract class CreateurEntite {
    public abstract Joueur creerJoueur(Partie partie, Context context);
    public abstract void creerPlatform(Partie partie, Context context);
}
