package pays_manin.projetiut.doodlesaute.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import pays_manin.projetiut.doodlesaute.R;
import pays_manin.projetiut.doodlesaute.modele.Utilisateur;

public class AjoutUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_user);
    }

    public void ajoutUser(View view) {
        EditText p = findViewById(R.id.registerPseudo);
        EditText m = findViewById(R.id.registerPassword);
        if (!(p == null || p.getText().toString().isEmpty() || m == null && m.getText().toString().isEmpty())) {
            Utilisateur u = new Utilisateur(p.getText().toString(), m.getText().toString());
            setResult(Activity.RESULT_OK, new Intent().putExtra("utilisateur", u));
            finish();
        }
    }
}