package pays_manin.projetiut.doodlesaute.modele.gestionCollision;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;
import pays_manin.projetiut.doodlesaute.modele.entite.Platform;
import pays_manin.projetiut.doodlesaute.modele.gestionDeplacement.GestionnaireDeplacement;
import pays_manin.projetiut.doodlesaute.view.GameView;

public class CollisionJoueur extends CollisionStandard {
    public CollisionJoueur(Entite entite) {
        super(entite);
    }

    @Override
    public boolean canMove(GestionnaireDeplacement deplacement, Partie partie) {
        for (Entite e : partie.getLesEntites()){
            if (this.entite == e) continue;

            double playerMinX = deplacement.getNewX();
            double playerMinY = deplacement.getNewY();
            double playerMaxX = deplacement.getNewX() + entite.getEntiteLargeur();
            double playerMaxY = deplacement.getNewY() + entite.getEntiteHauteur();

            double entMinX = e.getX();
            double entMinY = e.getY();
            double entMaxX = e.getX() + e.getEntiteLargeur();
            double entMaxY = e.getY() + e.getEntiteHauteur();

            if (playerMinX < entMaxX && playerMaxX > entMinX && playerMinY < entMaxY && playerMaxY > entMinY) {
                if (e instanceof Platform) {
                    partie.ajouterCollision(TypeCollision.COLLISION_JOUEUR_OBS, entite);
                }
                return true;
            }
        }

        if(!(deplacement.getNewY() + entite.getEntiteHauteur() <= GameView.hauteurEcran)) {
            partie.ajouterCollision(TypeCollision.COLLISION_JOUEUR_SOL, entite);
            return false;
        }


        return super.canMove(deplacement, partie);
    }
}
