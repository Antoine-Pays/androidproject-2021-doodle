package pays_manin.projetiut.doodlesaute.modele.entite;

import android.graphics.drawable.BitmapDrawable;

import pays_manin.projetiut.doodlesaute.modele.gestionCollision.CollisionJoueur;

public class Joueur extends Entite {

    private boolean move = true;

    public Joueur(BitmapDrawable bitmap, int x1, int y1, int x2, int y2) {
        super(bitmap, x1, y1);
        this.entiteLargeur = x2-x1;
        this.entiteHauteur = y2-y1;
        this.gravite = 10;
        this.laCollision = new CollisionJoueur(this);
    }

}
