package pays_manin.projetiut.doodlesaute.modele.gestionDeplacement;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;

public abstract class GestionnaireDeplacement {

    protected Partie partie;
    protected Direction orientation;
    protected float depGyroHorizontal;

    public GestionnaireDeplacement(Direction dir, Partie partie) {
        this.partie = partie;
        this.orientation = dir;
    }

    public abstract int getOldX();
    public abstract int getOldY();
    public abstract int getNewX();
    public abstract int getNewY();

    public abstract void deplacer(Entite e);
}
