package pays_manin.projetiut.doodlesaute.modele.gestionDeplacement;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;

public class DeplacementPlatforme extends DeplacementStandard {

    public DeplacementPlatforme(Partie partie) {
        super(Direction.BAS, partie);
    }

    @Override
    public void deplacer(Entite e) {

        oldX = e.getX();
        oldY = e.getY();
        newX = e.getX();
        newY = e.getY() + e.getGravite();

        if (e.getLaCollision().canMove(this, partie)){
            e.setX(newX);
            e.setY(newY);
        }

    }

}
