package pays_manin.projetiut.doodlesaute.modele.gestionnaireActions;

import android.content.Intent;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;
import pays_manin.projetiut.doodlesaute.view.ActivityPrincipale;

public class ActionsStandard extends GestionnaireActions {

    public ActionsStandard(Partie partie) {
        super(partie);
    }

    @Override
    public void actionCollObsMur(Entite entite) {
        partie.supprimerEntite(entite);
    }

    @Override
    public void actionCollJoueurSol(Entite entite) {
    }

    @Override
    public void actionCollJoueurObstacle(Entite entite) {
        Intent intent = new Intent(ActivityPrincipale.getGameView().getContext(), ActivityPrincipale.class);
        ActivityPrincipale.getGameView().getContext().startActivity(intent);
        ActivityPrincipale.getLeManager().stopBoucle();
    }
}
