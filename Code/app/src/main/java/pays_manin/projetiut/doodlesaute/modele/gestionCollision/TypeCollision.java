package pays_manin.projetiut.doodlesaute.modele.gestionCollision;

public enum TypeCollision {
    COLLISION_OBSTACLE_MUR,
    COLLISION_JOUEUR_SOL,
    COLLISION_JOUEUR_OBS
}
