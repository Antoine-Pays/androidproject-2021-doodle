package pays_manin.projetiut.doodlesaute.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import java.util.Iterator;
import java.util.Map;

import pays_manin.projetiut.doodlesaute.R;
import pays_manin.projetiut.doodlesaute.modele.Boucle;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;
import pays_manin.projetiut.doodlesaute.modele.entite.Joueur;
import pays_manin.projetiut.doodlesaute.modele.gestionCollision.TypeCollision;
import pays_manin.projetiut.doodlesaute.modele.gestionDeplacement.DeplacementJoueur;
import pays_manin.projetiut.doodlesaute.modele.gestionDeplacement.DeplacementPlatforme;
import pays_manin.projetiut.doodlesaute.modele.gestionDeplacement.Direction;
import pays_manin.projetiut.doodlesaute.modele.gestionDeplacement.GestionnaireDeplacement;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    private Direction directionVerticale;
    private float depGyroHorizontal;
    public static int largeurEcran;
    public static int hauteurEcran;
    private BitmapDrawable bitmapDrawable;
    private int cpt = 0;
    private SensorManager sensorManager;
    private Sensor gyroscopeSensor;


    public GameView(Context context, SensorManager sensorManager, Sensor gyroscopeSensor) {
        super(context);
        this.sensorManager = sensorManager;
        this.gyroscopeSensor = gyroscopeSensor;
        sensorGyro();
        getHolder().addCallback(this);
    }

    public void doDraw(Canvas canvas) {
        if (canvas == null) {
            return;
        }
        Drawable drawable = getResources().getDrawable(R.mipmap.imagefond);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        bitmapDrawable =  new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, largeurEcran, hauteurEcran,true));
        canvas.drawBitmap(bitmapDrawable.getBitmap(), 0, 0, null);
        for (Entite e : ActivityPrincipale.getLeManager().getPartie().getLesEntites()){
            e.draw(canvas);
        }
    }

    public void update() {

        if (cpt%120 == 0){
            ActivityPrincipale.getLeManager().getLeCreateur().creerPlatform(ActivityPrincipale.getLeManager().getPartie(), this.getContext());
        }

        DeplacementJoueur deplacementJoueur = new DeplacementJoueur(directionVerticale, depGyroHorizontal, ActivityPrincipale.getLeManager().getPartie());
        deplacementJoueur.deplacer(ActivityPrincipale.getLeManager().getJoueur());

        GestionnaireDeplacement deplacementPlatform = new DeplacementPlatforme(ActivityPrincipale.getLeManager().getPartie());
        for (Entite e : ActivityPrincipale.getLeManager().getPartie().getLesEntites()){
            if (e instanceof Joueur){ continue; }
            deplacementPlatform.deplacer(e);
        }

        Iterator<Map.Entry<TypeCollision, Entite>> iterator = ActivityPrincipale.getLeManager().getPartie().getListColl().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<TypeCollision, Entite> laMapColl = iterator.next();
            if (laMapColl.getKey() == TypeCollision.COLLISION_OBSTACLE_MUR) {
                ActivityPrincipale.getLeManager().getLeGestionnaireActions().actionCollObsMur(laMapColl.getValue());
            } else if (laMapColl.getKey() == TypeCollision.COLLISION_JOUEUR_SOL) {
                ActivityPrincipale.getLeManager().getLeGestionnaireActions().actionCollJoueurSol(laMapColl.getValue());
            } else if (laMapColl.getKey() == TypeCollision.COLLISION_JOUEUR_OBS) {
                ActivityPrincipale.getLeManager().getLeGestionnaireActions().actionCollJoueurObstacle(laMapColl.getValue());
            }
            iterator.remove();
        }

        cpt = cpt +1;
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        if (ActivityPrincipale.getLeManager().getBoucle().getState() == Thread.State.TERMINATED) {
            ActivityPrincipale.getLeManager().setBoucle(new Boucle(this));
        }
        ActivityPrincipale.getLeManager().getBoucle().setRunning(true);
        ActivityPrincipale.getLeManager().getBoucle().start();
    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
        boolean cycle = true;
        ActivityPrincipale.getLeManager().getBoucle().setRunning(false);
        while (cycle) {
            try {
                ActivityPrincipale.getLeManager().getBoucle().join();
                cycle = false;
            } catch (InterruptedException e) {
                Log.d("TEST", "Exception : " + e);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                directionVerticale = Direction.HAUT;
                break;

            case MotionEvent.ACTION_UP:
                directionVerticale = null;
        }

        return true;
    }

    protected void sensorGyro(){
        SensorEventListener gyroscopeSensorListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                depGyroHorizontal = sensorEvent.values[1] + depGyroHorizontal;
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
            }
        };
        sensorManager.registerListener(gyroscopeSensorListener, gyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {
        largeurEcran = width;
        hauteurEcran = height;
    }


}
