package pays_manin.projetiut.doodlesaute.modele.stub;

import java.util.ArrayList;

import pays_manin.projetiut.doodlesaute.modele.Utilisateur;

public class Stub {

    public static ArrayList<Utilisateur> creerUtilisateurs()   {
        Utilisateur max = new Utilisateur("max","max");
        Utilisateur anto  = new Utilisateur("antoine","anto");

        ArrayList<Utilisateur> mesJoueurs= new ArrayList<>();
        mesJoueurs.add(max);
        mesJoueurs.add(anto);


        return mesJoueurs;


    }
}
