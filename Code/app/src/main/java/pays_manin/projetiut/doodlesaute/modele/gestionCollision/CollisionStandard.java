package pays_manin.projetiut.doodlesaute.modele.gestionCollision;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;
import pays_manin.projetiut.doodlesaute.modele.gestionDeplacement.GestionnaireDeplacement;
import pays_manin.projetiut.doodlesaute.view.GameView;

public class CollisionStandard extends GestionnaireCollision {

    public CollisionStandard(Entite entite) {
        super(entite);
    }

    @Override
    public boolean canMove(GestionnaireDeplacement deplacement , Partie partie) {

        if(!(deplacement.getNewX() >= 0 && deplacement.getNewX() + entite.getEntiteLargeur() <= GameView.largeurEcran
                 && deplacement.getNewY() + entite.getEntiteHauteur() <= GameView.hauteurEcran)) {
            return false;
        }

        return true;
    }
}
