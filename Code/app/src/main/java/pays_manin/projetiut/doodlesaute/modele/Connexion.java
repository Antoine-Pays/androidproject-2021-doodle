package pays_manin.projetiut.doodlesaute.modele;

import java.util.ArrayList;

public class Connexion {
    private ArrayList<Utilisateur> mesUtilisateurs = new ArrayList<>();
    private DataBaseHelpeur db;

    public Connexion(DataBaseHelpeur db) {
        this.db = db;
        mesUtilisateurs = db.getUtilisateurs();
    }

    /**
     * Vérifie si l'utilisateur est bien contenu dans nos données
     * @param pseudo pseudo saisie par l'utilisateur
     * @param motDePasse mot de passe saisie par l'utilisateur
     * @return L'utilisateur si connecté
     */
    public  Utilisateur userConnecte(String pseudo, String motDePasse)
    {
        for ( Utilisateur u : mesUtilisateurs) {
            if(u.getPseudo().equals(pseudo) )
            {
                if(u.getMotDePasse().equals(motDePasse))
                    return u;
                else
                    return null;
            }
        }
        return null;
    }

    /**
     * Ajoute un utilisateur à la liste
     * @param u utilisateur à ajouter
     */
    public void ajouterUtilisateur(Utilisateur u)
    {
        mesUtilisateurs.add(u);
    }

    public ArrayList<Utilisateur> getMesUtilisateurs() {
        return mesUtilisateurs;
    }

    public void updateScore(Utilisateur u) {
        db.updateUtilisateurRecord(u);
    }
}
