package pays_manin.projetiut.doodlesaute.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;

import pays_manin.projetiut.doodlesaute.R;
import pays_manin.projetiut.doodlesaute.modele.Utilisateur;

public class TableauScoreActivity extends AppCompatActivity {

    private ListView listView;
    private CustomAdaptateur customAdaptater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tableau_score);

        listView = findViewById(R.id.tableauScore);
        ArrayList<Utilisateur> sortedUsers = sortScores(ActivityPrincipale.getLeManager().getCon().getMesUtilisateurs());

        customAdaptater = new CustomAdaptateur(getApplicationContext(), 0, sortedUsers);
        listView.setAdapter(customAdaptater);
    }

    public ArrayList<Utilisateur> sortScores(ArrayList<Utilisateur> users) {
        ArrayList<Utilisateur> returned = users;
        Collections.sort(users, (o1, o2) -> (o1.getRecord() > o2.getRecord() ?  o1.getRecord() : o2.getRecord()));

        return returned;
    }

}