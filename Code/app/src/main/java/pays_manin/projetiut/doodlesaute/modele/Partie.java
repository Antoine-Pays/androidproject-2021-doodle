package pays_manin.projetiut.doodlesaute.modele;

import java.util.ArrayList;
import java.util.HashMap;

import pays_manin.projetiut.doodlesaute.modele.entite.Entite;
import pays_manin.projetiut.doodlesaute.modele.gestionCollision.TypeCollision;
import pays_manin.projetiut.doodlesaute.view.ActivityPrincipale;

public class Partie {
    private ArrayList<Entite> lesEntites;


    private int pointEnCours;

    private HashMap<TypeCollision, Entite> listColl = new HashMap<>();

    public Partie() {
        lesEntites = new ArrayList<>();
        this.pointEnCours = 0;
    }

    public ArrayList<Entite> getLesEntites() {
        return lesEntites;
    }

    public void ajouterEntite(Entite entite) {
        lesEntites.add(entite);
    }

    public void supprimerEntite(Entite entite) {
        lesEntites.remove(entite);
        this.setPointEnCours(pointEnCours +1);
        ActivityPrincipale.getLeManager().setHighScore(pointEnCours);
    }

    public HashMap<TypeCollision, Entite> getListColl() {
        return listColl;
    }

    public void ajouterCollision(TypeCollision t, Entite entite) {
        listColl.put(t,entite);
    }

    public void setPointEnCours(int pointEnCours) {
        this.pointEnCours = pointEnCours;
    }
}
