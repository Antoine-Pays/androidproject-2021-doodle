package pays_manin.projetiut.doodlesaute.modele.gestionnaireActions;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;

public abstract class GestionnaireActions {

    protected Partie partie;

    public GestionnaireActions(Partie partie) {
        this.partie = partie;
    }

    public abstract void actionCollObsMur(Entite entite);
    public abstract void actionCollJoueurSol(Entite entite);
    public abstract void actionCollJoueurObstacle(Entite entite);
}
