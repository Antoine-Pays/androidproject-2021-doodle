package pays_manin.projetiut.doodlesaute.modele.gestionDeplacement;

public enum Direction {
    HAUT,
    BAS,
    GAUCHE,
    DROITE
}
