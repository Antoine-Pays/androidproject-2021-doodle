package pays_manin.projetiut.doodlesaute.modele.gestionCollision;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;
import pays_manin.projetiut.doodlesaute.modele.entite.Joueur;
import pays_manin.projetiut.doodlesaute.modele.gestionDeplacement.GestionnaireDeplacement;
import pays_manin.projetiut.doodlesaute.view.GameView;

public class CollisionPlatform extends CollisionStandard {
    public CollisionPlatform(Entite entite) {
        super(entite);
    }

    @Override
    public boolean canMove(GestionnaireDeplacement deplacement, Partie partie) {
        for (Entite e : partie.getLesEntites()) {
            if (this.entite == e) continue;

            double playerMinX = deplacement.getNewX();
            double playerMinY = deplacement.getNewY();
            double playerMaxX = deplacement.getNewX() + entite.getEntiteLargeur();
            double playerMaxY = deplacement.getNewY() + entite.getEntiteHauteur();

            double entMinX = e.getX();
            double entMinY = e.getY();
            double entMaxX = e.getX() + e.getEntiteLargeur();
            double entMaxY = e.getY() + e.getEntiteHauteur();

            if (playerMinX < entMaxX && playerMaxX > entMinX && playerMinY < entMaxY && playerMaxY > entMinY) {
                if (e instanceof Joueur) {
                    partie.ajouterCollision(TypeCollision.COLLISION_JOUEUR_OBS, e);
                }
                return true;
            }
        }


        //Vérifie si l'entité ici normalement un obstacle entre en collision avec un bord de l'application
        if(!(deplacement.getNewX() >= 0 && deplacement.getNewX() + entite.getEntiteLargeur() <= GameView.largeurEcran
                && deplacement.getNewY() >= 0 && deplacement.getNewY() + entite.getEntiteHauteur() <= GameView.hauteurEcran)) {
            partie.ajouterCollision(TypeCollision.COLLISION_OBSTACLE_MUR , entite);
            return false;
        }
        return super.canMove(deplacement, partie);
    }
}
