package pays_manin.projetiut.doodlesaute.modele;

import pays_manin.projetiut.doodlesaute.modele.createurs.CreateurEntite;
import pays_manin.projetiut.doodlesaute.modele.createurs.CreateurSimple;
import pays_manin.projetiut.doodlesaute.modele.entite.Joueur;
import pays_manin.projetiut.doodlesaute.modele.gestionnaireActions.ActionsStandard;
import pays_manin.projetiut.doodlesaute.modele.gestionnaireActions.GestionnaireActions;
import pays_manin.projetiut.doodlesaute.view.GameView;

public class Manager {
    private Boucle boucle;
    private Partie partie;
    private CreateurEntite leCreateur;
    private GestionnaireActions leGestionnaireActions;
    private Joueur joueur;
    private Connexion con;
    private Utilisateur userCourant;

    public Manager(GameView view, Connexion con) {
        partie = new Partie();
        leCreateur = new CreateurSimple();
        joueur = leCreateur.creerJoueur(partie, view.getContext());
        leGestionnaireActions = new ActionsStandard(partie);
        boucle = new Boucle(view);
        this.con = con;
    }

    public Boucle getBoucle() {
        return boucle;
    }
    public void setBoucle(Boucle boucle) {
        this.boucle = boucle;
    }

    public Partie getPartie() {
        return partie;
    }

    public CreateurEntite getLeCreateur() {
        return leCreateur;
    }

    public GestionnaireActions getLeGestionnaireActions() {
        return leGestionnaireActions;
    }

    public Joueur getJoueur() {
        return joueur;
    }

    public Connexion getCon() {
        return con;
    }

    public void setHighScore(int points) { getUserCourant().setRecord(points); }

    public Utilisateur getUserCourant() {
        return userCourant;
    }
    public void setEnCours(Utilisateur userCourant) {
        this.userCourant = userCourant;
    }

    public void stopBoucle() {
        boucle.setRunning(false);
        con.updateScore(userCourant);

    }

    public Boolean seConnecter(String pseudo, String mdp)
    {
        Utilisateur user = con.userConnecte(pseudo,mdp);
        if(user !=null)
        {
            this.setEnCours(user);
            return true;
        }
        return false;

    }
}
