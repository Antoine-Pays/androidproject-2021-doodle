package pays_manin.projetiut.doodlesaute.modele;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DataBaseHelpeur extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Utilisateurs";
    private static final int DATABASE_VERSION = 1;

    public DataBaseHelpeur(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String TABLE = "CREATE TABLE utilisateur" +
                "(" +
                "pseudo TEXT," +
                "mdp TEXT," +
                "record INTEGER"+
                ")";

        db.execSQL(TABLE);
    }

    //TODO: On perd la totalité des contacts à un changement de version, à changer
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS contact");
            onCreate(db);
        }
    }

    public ArrayList<Utilisateur> getUtilisateurs() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Utilisateur> returned = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM utilisateur", null);
        if (cursor == null) return null;

        if (cursor.moveToFirst()) {
            do {
                //String nom, String prenom, String date, String phone, String email, boolean sex, String avatar) {
                Utilisateur utilisateur = new Utilisateur(
                        cursor.getString(cursor.getColumnIndex("pseudo")),
                        cursor.getString(cursor.getColumnIndex("mdp")),
                        cursor.getInt(cursor.getColumnIndex("record")));

                returned.add(utilisateur);
            } while (cursor.moveToNext());
        }
        return returned;
    }

    public void addUtilisateur(Utilisateur utilisateur) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("INSERT INTO utilisateur (pseudo, mdp, record) VALUES('"+utilisateur.getPseudo()+"', '"+utilisateur.getMotDePasse()+"', '"+utilisateur.getRecord()+"')");
    }

    public  void updateUtilisateurRecord(Utilisateur utilisateur) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE utilisateur SET record = '"+utilisateur.getRecord()+"' WHERE pseudo = '"+utilisateur.getPseudo()+"' AND record < '"+utilisateur.getRecord()+"' ");
    }

    /*


    public void removeContact(Contact c) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM contact WHERE nom='"+c.getNom()+"' AND prenom='"+c.getPrenom()+"'");
    }



     */
}
