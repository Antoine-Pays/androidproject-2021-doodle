package pays_manin.projetiut.doodlesaute.modele.entite;

import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;

import pays_manin.projetiut.doodlesaute.modele.gestionCollision.CollisionStandard;
import pays_manin.projetiut.doodlesaute.modele.gestionCollision.GestionnaireCollision;

public abstract class Entite {

    protected BitmapDrawable img = null;
    protected int x,y;
    protected int entiteHauteur, entiteLargeur;

    protected static final int VITESSE = 10;
    protected int vitesseX = VITESSE, vitesseY = VITESSE;

    protected int gravite = 5;

    protected GestionnaireCollision laCollision;


    public Entite(BitmapDrawable bitmap, int x, int y) {
        this.x = x;
        this.y = y;
        this.img = bitmap;
        this.laCollision = new CollisionStandard(this);
    }

    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }

    public int getEntiteHauteur() {
        return entiteHauteur;
    }
    public int getEntiteLargeur() {
        return entiteLargeur;
    }


    public int getVitesseX() {
        return vitesseX;
    }
    public int getVitesseY() {
        return vitesseY;
    }

    public int getGravite() {
        return gravite;
    }

    public GestionnaireCollision getLaCollision() {
        return laCollision;
    }


    public void draw(Canvas canvas) {
        if (img == null) {
            return;
        }
        canvas.drawBitmap(img.getBitmap(), x, y, null);
    }
}