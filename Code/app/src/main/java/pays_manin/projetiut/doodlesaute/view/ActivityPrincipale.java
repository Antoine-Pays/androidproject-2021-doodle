package pays_manin.projetiut.doodlesaute.view;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import pays_manin.projetiut.doodlesaute.R;
import pays_manin.projetiut.doodlesaute.modele.Connexion;
import pays_manin.projetiut.doodlesaute.modele.DataBaseHelpeur;
import pays_manin.projetiut.doodlesaute.modele.Manager;
import pays_manin.projetiut.doodlesaute.modele.Utilisateur;

public class ActivityPrincipale extends AppCompatActivity {

    private static GameView gameView;
    private static Connexion connexion;
    private static Manager leManager;

    public DataBaseHelpeur db = new DataBaseHelpeur(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        gameView = new GameView(this, sensorManager, gyroscopeSensor);

        connexion= new Connexion(db);
        leManager = new Manager(gameView, connexion);

        setContentView(R.layout.activite_menu);
    }

    public void debutJeu(View view) {
        EditText p = findViewById(R.id.text_box_pseudo);
        EditText m = findViewById(R.id.text_box_password);
        if (leManager.seConnecter(p.getText().toString(), m.getText().toString())) {
            setContentView(gameView);
            if (gameView == null){
                setContentView(R.layout.activite_menu);
            }
        }
    }

    public static GameView getGameView() {
        return gameView;
    }

    public static Manager getLeManager() {
        return leManager;
    }

    public void tableauScore(View view) {
        Intent intent = new Intent(this.getApplicationContext(), TableauScoreActivity.class);
        startActivity(intent);
    }

    public void creationUser(View view) {
        Intent intent = new Intent(this.getApplicationContext(), AjoutUserActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            Utilisateur u = (Utilisateur) data.getSerializableExtra("utilisateur");
            connexion.ajouterUtilisateur(u);
            db.addUtilisateur(u);
        }
    }

    /*

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("TEST", "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("TEST", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("TEST", "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TEST", "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TEST", "onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("TEST", "onRestart");
    }

    */

}
