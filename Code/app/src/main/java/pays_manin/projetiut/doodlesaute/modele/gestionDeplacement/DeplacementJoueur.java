package pays_manin.projetiut.doodlesaute.modele.gestionDeplacement;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;

public class DeplacementJoueur extends DeplacementStandard {



    public DeplacementJoueur(Direction dir, float depGyroHorizontal, Partie partie) {
        super(dir, partie);
        this.depGyroHorizontal = depGyroHorizontal;
    }


    @Override
    public void deplacer(Entite e) {

        oldX = e.getX();
        oldY = e.getY();
        newX = (orientation == Direction.GAUCHE || orientation == Direction.DROITE) ? (orientation == Direction.GAUCHE ? e.getX() - e.getVitesseX() : e.getX() + e.getVitesseX()) : e.getX();
        newY = (orientation == Direction.HAUT || orientation == Direction.BAS) ? (orientation == Direction.HAUT ? e.getY() - e.getVitesseY() : e.getY() + e.getVitesseY()) : e.getY();

        newX = (int) ( oldX + depGyroHorizontal * 10 );

        if (orientation != Direction.HAUT) {
            newY = newY + e.getGravite();
        }

        if (e.getLaCollision().canMove(this, partie)){
            e.setX(newX);
            e.setY(newY);
        }

    }
}
