package pays_manin.projetiut.doodlesaute.modele.gestionCollision;

import pays_manin.projetiut.doodlesaute.modele.Partie;
import pays_manin.projetiut.doodlesaute.modele.entite.Entite;
import pays_manin.projetiut.doodlesaute.modele.gestionDeplacement.GestionnaireDeplacement;

public abstract class GestionnaireCollision {

    protected Entite entite;

    public GestionnaireCollision(Entite entite) {
        this.entite = entite;
    }

    public abstract boolean canMove(GestionnaireDeplacement deplacement, Partie partie);

}
